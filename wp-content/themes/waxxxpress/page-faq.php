<?php
/**
 * Template Name: FAQ
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

	
<!--//Content-->

	<div id="frame-content">
		<div class="wrapper">
		
			<!--//Breadcrumbs-->
			
				<div id="breadcrumbs" class="wrapper">
					<?php if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<ul id="breadcrumb-list"><li>','</li></ul>');
					} ?>
				</div>
			
			<!--//End Breadcrumbs-->
			
			<!--//Categories-->
			
				<div id="subpage-container">
					<div id="left-column">
						<div class="content">
							<h1><?php the_title(); ?></h1>
							<h2><?php the_field('sub_title'); ?></h2>
							<div id="faq-links" class="clear">
								<h3>Products</h3>
								<ul>
									<?php $recent = new WP_Query("cat=12&showposts=100"); while($recent->have_posts()) : $recent->the_post();?>
										<li><a href="#<?php the_id(); ?>"><?php the_title(); ?></a></li>
									<?php endwhile; ?>
								</ul>
								<h3>Accounts</h3>
								<ul>
									<?php $recent = new WP_Query("cat=13&showposts=100"); while($recent->have_posts()) : $recent->the_post();?>
										<li><a href="#<?php the_id(); ?>"><?php the_title(); ?></a></li>
									<?php endwhile; ?>
								</ul>
								<h3>Ordering</h3>
								<ul>
									<?php $recent = new WP_Query("cat=14&showposts=100"); while($recent->have_posts()) : $recent->the_post();?>
										<li><a href="#<?php the_id(); ?>"><?php the_title(); ?></a></li>
									<?php endwhile; ?>
								</ul>
								<h3>Delivery</h3>
								<ul>
									<?php $recent = new WP_Query("cat=16&showposts=100"); while($recent->have_posts()) : $recent->the_post();?>
										<li><a href="#<?php the_id(); ?>"><?php the_title(); ?></a></li>
									<?php endwhile; ?>
								</ul>
							</div>
							<div id="faq-sections">
								<h3>Products</h3>
								<ul>
									<?php $recent = new WP_Query("cat=12&showposts=100"); while($recent->have_posts()) : $recent->the_post();?>
										<li>
											<div id="<?php the_id(); ?>" class="faq-title"><?php the_title(); ?></div>
											<div class="faq-copy">
												<?php the_content(); ?>
												<a href="#faq-links">back to top</a>
											</div>	
										</li>
									<?php endwhile; ?>
								</ul>
								<h3>Accounts</h3>
								<ul>
									<?php $recent = new WP_Query("cat=13&showposts=100"); while($recent->have_posts()) : $recent->the_post();?>
										<li>
											<div id="<?php the_id(); ?>" class="faq-title"><?php the_title(); ?></div>
											<div class="faq-copy">
												<?php the_content(); ?>
												<a href="#faq-links">back to top</a>
											</div>	
										</li>
									<?php endwhile; ?>
								</ul>
								<h3>Ordering</h3>
								<ul>
									<?php $recent = new WP_Query("cat=14&showposts=100"); while($recent->have_posts()) : $recent->the_post();?>
										<li>
											<div id="<?php the_id(); ?>" class="faq-title"><?php the_title(); ?></div>
											<div class="faq-copy">
												<?php the_content(); ?>
												<a href="#faq-links">back to top</a>
											</div>	
										</li>
									<?php endwhile; ?>
								</ul>
								<h3>Delivery</h3>
								<ul>
									<?php $recent = new WP_Query("cat=16&showposts=100"); while($recent->have_posts()) : $recent->the_post();?>
										<li>
											<div id="<?php the_id(); ?>" class="faq-title"><?php the_title(); ?></div>
											<div class="faq-copy">
												<?php the_content(); ?>
												<a href="#faq-links">back to top</a>
											</div>	
										</li>
									<?php endwhile; ?>
								</ul>
							</div>
						</div>
					</div>
					<div id="sidebar">
						<?php include("sidebar-101.php"); ?>
					</div>
					<div class="clear"></div>
				</div>
			
			<!--//End Categories-->

		</div>
	</div>
		
<!--//End Content-->
		

<?php get_footer(); ?>