<?php
/**
 * Template Name: Register Custom
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>


	
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
				
					<!--//Breadcrumbs-->
					
						<div id="breadcrumbs" class="wrapper">
							<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ul id="breadcrumb-list"><li>','</li></ul>');
							} ?>
						</div>
					
					<!--//End Breadcrumbs-->
					
					<!--//Categories-->
					
						<div id="subpage-container">
							<div id="left-column">
								<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
									<h1><?php the_title(); ?></h1>
									<h2><?php the_field('sub_title'); ?></h2>
									<div class="clear"></div>
									<?php global $user_ID; if( $user_ID ) : ?>
									<?php if( current_user_can('level_10') ) : ?>
									
										<?php the_content(); ?>
									
									<?php else : ?>
									<?php endif; ?>
									<?php endif; ?>
								<?php endwhile; ?>
							</div>
							<div id="sidebar">
								<?php get_sidebar(); ?>
							</div>
							<div class="clear"></div>
						</div>
					
					<!--//End Categories-->

				</div>
			</div>
				
		<!--//End Content-->
		


<?php get_footer(); ?>