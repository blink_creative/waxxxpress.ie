

<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the wordpress construct of pages
 * and that other 'pages' on your wordpress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
				
					<!--//Content-->
					
						<?php the_content(); ?>
					
					<!--End-->						
					
				</div>
			</div>
				
		<!--//End Content-->
		
	<?php endwhile; ?>
<?php get_footer(); ?>

