<?php
/**
 * Template Name: Reviews
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */
?>

	<!--Stylesheet-->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/reviews.css" />
	<link rel='stylesheet' id='wp-customer-reviews-css'  href='http://www.waxxxpress.com/wp-content/plugins/wp-customer-reviews/wp-customer-reviews.css?ver=2.4.2' type='text/css' media='all' />

	<!-- Scripts -->
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/jquery-core.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-cufon.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fonts/font-nouvelle.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fonts/font-akzidenz-cn.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fonts/font-hoefler.js"></script>
	<script type='text/javascript' src='http://www.waxxxpress.com/wp-content/plugins/wp-customer-reviews/wp-customer-reviews.js?ver=2.4.2'></script>	
	<script type="text/javascript">
	$(document).ready(function() {
						   
 
	/*--Cufon--*/
	
	Cufon.replace('.akzidenz, #cart table th, #goodbuys li .goodbuys-copy h2, #goodbuys li .goodbuys-copy p, #checkout h3, #left-column h1, .button-rect-black, .product-list li .title h2, .product-list li .title h3, .product-details h3, .product-details h5, h4, .form-button button, #product-menu li, #product-cart label, #product-details h1, #product-details h3, #breadcrumb-list li, #site-cart, .drop-menu > li > a, .sf-menu > li > a,#navigation-footer li, .button-rect-pink, .button-rect-white, .button-rect-purple, .button-rect-green, #homepage-cta h2, #pointer-cta h3', { fontFamily: 'Akzidenz',hover: true });
	Cufon.replace('.nouvelle', { fontFamily: 'Nouvelle' });
	Cufon.replace('.hoefler, #left-column h2, .product-details h4, #product-details h2', { fontFamily: 'Hoefler', hover:true });
	

	
 
});

	</script>


<?php the_content(); ?>
