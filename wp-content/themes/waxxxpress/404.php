<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
				
					<!--//Breadcrumbs-->
					
						<div id="breadcrumbs" class="wrapper">
							<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ul id="breadcrumb-list"><li>','</li></ul>');
							} ?>
						</div>
					
					<!--//End Breadcrumbs-->
					
					<!--//Categories-->
					
						<div id="subpage-container">
							<div id="left-column">
								<div class="content">
									<h1>Well, you're definately lost.</h1>
									<h2>Don't sweat it, let's get you back on track.</h2>
									<p>Just use the navigation above or the site search on the top right of the website to find what you're looking for!</p>
								</div>
							</div>
							<div id="sidebar">
								<?php get_sidebar(); ?>
							</div>
							<div class="clear"></div>
						</div>
					
					<!--//End Categories-->

				</div>
			</div>
				
		<!--//End Content-->


<?php get_footer(); ?>