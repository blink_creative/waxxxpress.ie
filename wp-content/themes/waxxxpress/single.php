<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

	
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
				
					<!--//Breadcrumbs-->
					
						<div id="breadcrumbs" class="wrapper">
							<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ul id="breadcrumb-list"><li>','</li></ul>');
							} ?>
						</div>
					
					<!--//End Breadcrumbs-->
					
					<!--//Categories-->
					
						<div id="subpage-container">
							<div id="left-column">
								<div class="content">
									<div class="pager">
										<ul>
											<li class="float-left"><?php previous_post_link( '%link', '' . _x( '', 'Previous post link', 'twentyten' ) . ' Previous Post' ); ?></li>
											<li class="float-right"><?php next_post_link( '%link', 'Next Post ' . _x( '', 'Next post link', 'twentyten' ) . '' ); ?></li>
										</ul>
									</div>
									<div class="blog-content">
										<h1><?php the_title(); ?></h1>
										<?php if (has_post_thumbnail( $post->ID ) ): ?>
											<div class="section-image">
												<?php the_post_thumbnail('full'); ?>
											</div>
										<?php endif; ?>                            
										<p><?php twentyten_posted_on(); ?></p>
										<?php the_content(); ?>
									</div>
								</div>
							</div>
							<div id="sidebar">
								<?php include('sidebar-blog.php'); ?>
							</div>
						</div>
					
					<!--//End Categories-->

				</div>
			</div>
				
		<!--//End Content-->
		

<?php get_footer(); ?>
