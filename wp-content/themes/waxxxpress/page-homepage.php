<?php
/**
 * Template Name: Homepage
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			
	<!--//Content-->
	
		<div id="frame-content">
			<div class="wrapper">
				<div id="fresh"></div>
				<div id="animation-heart">
					<div class="button-rect-green">
						<a href="<?php echo get_permalink(52); ?>">WHY USE US!</a>
					</div>
				</div>
				<div id="animation-television"><a href="<?php echo get_permalink(57); ?>">View video</a></div>
				<div id="animation-bridge"></div>
				<div id="splash" class="circle">
					<ul class="circle">
						<li class="plane">
							<div class="splash-copy">
								<h1><span class="akzidenz">Welcome</span> <span class="hoefler">to Waxxxpress</span></h1>
								<p>Welcome aboard Waxxxpress.ie this is a direct service delivering salon waxxx exxxpress to your door!</p>
								<div class="button-rect-white">
									<a href="http://www.waxxxpress.com/?page_id=60">Shop Now</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div id="animation-koala"></div>
				<div id="animation-pointer">
					<div id="pointer-image"><a href="<?php echo get_permalink(57); ?>">View our video</a></div>
					<div id="pointer-cta">
						<h3><span class="overflow-text">Which WAXXX</span><span class="block">is right 4 u?</span></h3>
						<div class="button-rect-purple">
							<a href="<?php echo get_permalink(54); ?>">Go to waxxxselector</a>
						</div>
					</div>
				</div>
				<div id="homepage-cta">
					<div class="float-left" id="cta-app">
						<h2><span class="overflow-text text-blue">BLOG! BLOG!</span><span class="overflow-text block">the waxxxet</span><span class="block">collective</span></h2>
						<div class="button-rect-blue"><a href="<?php echo get_permalink(27); ?>">Take me there</a></div>
					</div>
					<div class="float-left" id="cta-hints">
						<h2><span class="overflow-text text-white">Waxxxing 101</span><span class="overflow-text block text-black">training</span><span class="block text-black">&amp; courses</span></h2>
						<div class="button-rect-white"><a href="<?php echo get_permalink(61); ?>">Tell Me More!</a></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>

<?php endwhile; ?>
<?php get_footer(); ?>