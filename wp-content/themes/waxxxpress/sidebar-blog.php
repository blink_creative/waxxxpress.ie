<div id="subpage-sidebar">
	<div id="blog-lists">
		<ul>
			<li><h3>Recent Posts</h3>
				<ul>
					<?php $recent = new WP_Query(); ?>
					<?php $recent->query('cat=8,9,10&showposts=5'); ?>
					<?php while($recent->have_posts()) : $recent->the_post(); ?>
					<li>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?> - <?php the_date('d M y'); ?></a>
					</li>
					<?php endwhile; ?>
				</ul>
			</li>
		</ul>
	</div>
	<div id="sidebar-shop">
		<a href="<?php shopp('catalog','url'); ?>">Shop Now</a>
	</div>
</div>