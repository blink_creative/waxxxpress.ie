Content-type: text/html; charset=utf-8
From: <?php shopp('purchase','email-from'); ?>
To: <?php shopp('purchase','email-to'); ?>
Subject: <?php shopp('purchase','email-subject'); ?>

<html>

<table cellpadding="0" cellspacing="0" bgcolor="#efefef" align="center" width="100%">
	<tr>
		<td>
			<table style="border-bottom:2px dotted #e01881;" width="600" align="center" bgcolor="#FFFFFF" cellpadding="20" cellspacing="0">
				<tr>
					<td>
						<div id="header">
							<table cellpadding="0" cellspacing="0" bgcolor="#ffffff">
								<tr>
									<td width="301"><img src="<?php bloginfo('template_url'); ?>/images/emails/ready.gif" width="297" height="174" /></td>
									<td width="255" style="padding:20px 0 0;" valign="top">
										<p style="text-align:right; color:#E10981; font-family:'Courier New', Courier, monospace; font-size:12px;">
											BAAM Cosmetics<br />
											1 Willington Avenue<br />
											Templeogue<br />
											Dublin 6W<br />
											01 4507573 / 087 2427510<br />
											<br />
												<a href="mailto:info@baamcosmetics.ie" style="color:#E10981; text-decoration:none;">info@baamcosmetics.ie</a><br />
											VAT No. IE3971499K
										</p>
									</td>
								</tr>
							</table>
							<div class="content">
								<p style="color:#555555; font-family:'Courier New', Courier, monospace; font-size:12px;">Hi <?php shopp('purchase','firstname'); ?>,</p>
								<p style="color:#555555; font-family:'Courier New', Courier, monospace; font-size:12px;">Your order has been processed and below is confirmation of all your in-flight itinerary details. </p>
								<p style="color:#555555; font-family:'Courier New', Courier, monospace; font-size:12px;">To ensure your Waxxxpress experience is fast and turbulence free, our propeller-powered staff pick, pack and despatch your order with the speed and efficiency of a turbo jet. </p>
								<p style="color:#555555; font-family:'Courier New', Courier, monospace; font-size:12px;">To view your order and progress status, simply log into your Waxxxpress account where you can keep track of all the details from take-off to touch-down.</p>
								<p style="color:#555555; font-family:'Courier New', Courier, monospace; font-size:12px;">So sit back, relax and enjoy the wax ride.</p>
								<p style="color:#555555; font-family:'Courier New', Courier, monospace; font-size:12px;"><span>xxx</span></p>
							</div>
						</div>
						<div id="body">
							<?php shopp('purchase','receipt'); ?>
							<?php if (shopp('purchase','notpaid') && shopp('checkout','get-offline-instructions')): ?>
								<p><?php shopp('checkout','offline-instructions'); ?></p>
							<?php endif; ?>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
					
		
</html>