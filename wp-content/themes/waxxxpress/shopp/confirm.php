<!--//Breadcrumbs-->

<div id="breadcrumbs" class="wrapper">
	<?php shopp('catalog','breadcrumb'); ?>
</div>

<!--//End Breadcrumbs-->

<!--//Product-->

<div id="subpage-container">
	<div id="left-column">

		<?php shopp('checkout','cart-summary'); ?>
		
		<form action="<?php shopp('checkout','url'); ?>" method="post" class="shopp" id="checkout">
			<?php shopp('checkout','function','value=confirmed'); ?>
			<p class="submit"><?php shopp('checkout','confirm-button','value=Pay with PayPal'); ?></p>
		</form>
		
	</div>
	<div id="sidebar">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>

<!--//End Product-->
