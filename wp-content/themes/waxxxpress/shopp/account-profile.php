<!--//Breadcrumbs-->

<div id="breadcrumbs" class="wrapper">
	<?php shopp('catalog','breadcrumb'); ?>
</div>

<!--//End Breadcrumbs--> 

<!--//Product-->

<div id="subpage-container">
	<div id="left-column">
		<form id="checkout" action="<?php shopp('customer','action'); ?>" method="post" class="shopp validate" autocomplete="off">
			<h1>Your Account</h1>
			<h3>Edit your details</h3>
			<?php if(shopp('customer','errors-exist')) shopp('customer','errors'); ?>
			<?php if(shopp('customer','password-changed')): ?>
			<div class="notice">Your password has been changed successfully.</div>
			<?php endif; ?>
			<?php if(shopp('customer','profile-saved')): ?>
			<div class="notice">Your account has been updated.</div>
			<?php endif; ?>
			<div class="left-form">
				<label for="firstname">First Name</label>
				<?php shopp('customer','firstname','required=true&minlength=2&size=8&title=First Name'); ?>
			</div>
			<div class="right-form">
				<label for="lastname">Last Name</label>
				<?php shopp('customer','lastname','required=true&minlength=3&size=14&title=Last Name'); ?>
			</div>
			<div class="clear"></div>	
			<div class="left-form">
				<label for="company">Company</label>
				<?php shopp('customer','company','size=20&title=Company'); ?>
			</div>
			<div class="right-form">
				<label for="phone">Phone</label>
				<?php shopp('customer','phone','format=phone&size=15&title=Phone'); ?>
			</div>
			<div class="clear"></div>	
			<div class="left-form">
				<label for="email">Email</label>
				<?php shopp('customer','email','required=true&format=email&size=30&title=Email'); ?>
			</div>
			<div class="right-form">
				<label for="phone">Phone</label>
				<?php shopp('customer','phone','format=phone&size=15&title=Phone'); ?>
			</div>
			<div class="clear"></div>	
			<label for="marketing"><?php shopp('customer','marketing','title=I would like to continue receiving e-mail updates and special offers!'); ?> I would like to continue receiving e-mail updates and special offers!</label>
				
			<?php while (shopp('customer','hasinfo')): ?>
				<div class="left-form">
					<label><?php shopp('customer','info','mode=name'); ?></label>
					<?php shopp('customer','info'); ?>
				</div>
				<div class="clear"></div>	
			<?php endwhile; ?>
			
			<h3>Change Your Password</h3>
			<div class="left-form">
				<label for="password">Password</label>
				<?php shopp('customer','password','size=14&title=New Password'); ?>
			</div>
			<div class="right-form">
				<label for="confirm-password">Confirm Password</label>
				<?php shopp('customer','confirm-password','&size=14&title=Confirm Password'); ?>
			</div>
			<div class="clear"></div>	
			<div class="form-button"><?php shopp('customer','save-button','label=Save'); ?></div>
			<div class="clear"></div>	
			<p><a href="<?php shopp('customer','url'); ?>">&laquo; Return to Account Management</a></p>
		</form>
	</div>
	<div id="sidebar">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>
