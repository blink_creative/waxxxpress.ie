<!--//Breadcrumbs-->

<div id="breadcrumbs" class="wrapper">
	<?php shopp('catalog','breadcrumb'); ?>
</div>

<!--//End Breadcrumbs--> 

<!--//Product-->

<div id="subpage-container">
	<div id="left-column">
		<div id="checkout">
	
		<?php shopp('checkout','cart-summary'); ?>
	
		<?php if(shopp('customer','notloggedin')): ?>
		<form action="<?php shopp('checkout','url'); ?>" method="post" class="shopp login-check" id="login">
				<h3>Account Login</h3>
				<div class="left-form">
					<label for="login">Email Address</label>
					<input type="text" name="account-login" id="account-login" size="20" title="Login" autocomplete="off" />
				</div>
				<div class="right-form">
					<label for="password">Password</label>
					<input type="password" name="password-login" id="password-login" size="20" title="Password" autocomplete="off" />
				</div>
				<input type="hidden" name="process-login" value="true" />
				<input type="hidden" name="redirect" value="<?php shopp('checkout','url'); ?>" />
				<div class="form-button login-btn">
					<button type="submit" name="submit-login" id="submit-login">Login</button>
				</div>
				<div class="clear"></div>
				<p><a href="<?php shopp('customer','url'); ?>/?page_id=63&acct=recover">Lost your password?</a></p>
		</form>
		<?php endif; ?>
	
		<form action="<?php shopp('checkout','url'); ?>" method="post" class="shopp validate">
		
			<?php if (shopp('cart','hasitems')): ?>
			<?php shopp('checkout','function'); ?>
			
			<!--Contact Info-->
			
			<h3>Contact Information</h3>
			<div class="left-form">
				<label for="firstname">First Name</label>
				<?php shopp('checkout','firstname','required=true&minlength=2&size=8&title=First Name'); ?>
			</div>
			<div class="right-form">
				<label for="lastname">Last Name</label>
				<?php shopp('checkout','lastname','required=true&minlength=3&size=14&title=Last Name'); ?>
			</div>
			<div class="clear"></div>
			<div class="left-form">
				<label for="company">Company/Organization</label>
				<?php shopp('checkout','company','size=22&title=Company/Organization'); ?>
			</div>
			<div class="right-form">
				<label for="phone">Phone</label>
				<?php shopp('checkout','phone','format=phone&size=15&title=Phone'); ?>
			</div>
			<div class="clear"></div>
			<div class="left-form">
				<label for="email">Email</label>
				<?php shopp('checkout','email','required=true&format=email&size=30&title=Email'); ?>
			</div>
			<?php if (shopp('customer','notloggedin')): ?>
			<div class="right-form">
				<label for="password">Password</label>
				<?php shopp('checkout','password','required=true&format=passwords&size=16&title=Password'); ?>
			</div>
			<div class="clear"></div>
			<div class="left-form">
				<label for="confirm-password">Confirm Password</label>
				<?php shopp('checkout','confirm-password','required=true&format=passwords&size=16&title=Password Confirmation'); ?>
			</div>
			<?php endif; ?>
			<div class="clear"></div>
			<div class="checkboxes">
				<label for="marketing"><?php shopp('checkout','marketing'); ?> <?php _e('Yes, I would like to receive e-mail updates and special offers!','Shopp'); ?></label>
			</div>
			
			<!--Billing Info-->
			
			<?php if (shopp('cart','needs-shipped')): ?>
			<?php else: ?>
			<?php endif; ?>
			<h3>Billing Address</h3>
			<div class="left-form">
				<label for="billing-address">Street Address</label>
				<?php shopp('checkout','billing-address','required=true&title=Billing street address'); ?>
			</div>
			<div class="right-form">
				<label for="billing-xaddress">Address Line 2</label>
				<?php shopp('checkout','billing-xaddress','title=Billing address line 2'); ?>
			</div>
			<div class="clear"></div>
			<div class="left-form">
				<label for="billing-city">City</label>
				<?php shopp('checkout','billing-city','required=true&title=City billing address'); ?>
			</div>
			<div class="right-form">
				<label for="billing-state">County / Province</label>
				<select class="required billing-state disabled" title="State/Provice/Region billing address" id="billing-state-menu" name="billing[state]">
					<option value="">
						<?php shopp('checkout','billing-state','mode=value'); ?>
					</option>
					<option value="Antrim">Antrim</option>
					<option value="Armagh">Armagh</option>
					<option value="Carlow">Carlow</option>
					<option value="Cavan">Cavan</option>
					<option value="Clare">Clare</option>
					<option value="Cork">Cork</option>
					<option value="Derry">Derry</option>
					<option value="Donegal">Donegal</option>
					<option value="Down">Down</option>
					<option value="Dublin">Dublin</option>
					<option value="Fermanagh">Fermanagh</option>
					<option value="Galway">Galway</option>
					<option value="Kerry">Kerry</option>
					<option value="Kildare">Kildare</option>
					<option value="Kilkenny">Kilkenny</option>
					<option value="Laois">Laois</option>
					<option value="Leitrim">Leitrim</option>
					<option value="Limerick">Limerick</option>
					<option value="Longford">Longford</option>
					<option value="Louth">Louth</option>
					<option value="Mayo">Mayo</option>
					<option value="Meath">Meath</option>
					<option value="Monaghan">Monaghan</option>
					<option value="Offaly">Offaly</option>
					<option value="Roscommon">Roscommon</option>
					<option value="Sligo">Sligo</option>
					<option value="Tipperary">Tipperary</option>
					<option value="Tyrone">Tyrone</option>
					<option value="Waterford">Waterford</option>
					<option value="Westmeath">Westmeath</option>
					<option value="Wexford">Wexford</option>
					<option value="Wicklow">Wicklow</option>
				</select>
			</div>
			<div class="clear"></div>
			<div class="left-form">
				<label for="billing-country">Country</label>
				<?php shopp('checkout','billing-country','required=true&title=Country billing address'); ?>
				<div class="none"><input type="text" value="n/a" title="Postal/Zip Code billing address" id="billing-postcode" name="billing[postcode]"></div>
			</div>
			<div class="clear"></div>
			<?php if (shopp('cart','needs-shipped')): ?>
			<h3>Shipping Address</h3>
			<div class="checkboxes">
				<?php shopp('checkout','same-shipping-address'); ?>
			</div>
			<div class="left-form">
				<label for="shipping-address">Street Address</label>
				<?php shopp('checkout','shipping-address','required=true&title=Shipping street address'); ?>
			</div>
			<div class="right-form">
				<label for="shipping-xaddress">Address Line 2</label>
				<?php shopp('checkout','shipping-xaddress','title=Shipping address line 2'); ?>
			</div>
			<div class="clear"></div>
			<div class="left-form">
				<label for="shipping-city">City</label>
				<?php shopp('checkout','shipping-city','required=true&title=City shipping address'); ?>
			</div>
			<div class="right-form">
				<label for="shipping-state">County / Province</label>
				<select class="required shipping-state disabled" title="State/Provice/Region shipping address" id="shipping-state-menu" name="shipping[state]">
					<option value="">
						<?php shopp('checkout','billing-state','mode=value'); ?>
					</option>
					<option value="Antrim">Antrim</option>
					<option value="Armagh">Armagh</option>
					<option value="Carlow">Carlow</option>
					<option value="Cavan">Cavan</option>
					<option value="Clare">Clare</option>
					<option value="Cork">Cork</option>
					<option value="Derry">Derry</option>
					<option value="Donegal">Donegal</option>
					<option value="Down">Down</option>
					<option value="Dublin">Dublin</option>
					<option value="Fermanagh">Fermanagh</option>
					<option value="Galway">Galway</option>
					<option value="Kerry">Kerry</option>
					<option value="Kildare">Kildare</option>
					<option value="Kilkenny">Kilkenny</option>
					<option value="Laois">Laois</option>
					<option value="Leitrim">Leitrim</option>
					<option value="Limerick">Limerick</option>
					<option value="Longford">Longford</option>
					<option value="Louth">Louth</option>
					<option value="Mayo">Mayo</option>
					<option value="Meath">Meath</option>
					<option value="Monaghan">Monaghan</option>
					<option value="Offaly">Offaly</option>
					<option value="Roscommon">Roscommon</option>
					<option value="Sligo">Sligo</option>
					<option value="Tipperary">Tipperary</option>
					<option value="Tyrone">Tyrone</option>
					<option value="Waterford">Waterford</option>
					<option value="Westmeath">Westmeath</option>
					<option value="Wexford">Wexford</option>
					<option value="Wicklow">Wicklow</option>
				</select>
			</div>
			<div class="clear"></div>
			<div class="left-form">
				<label for="shipping-country">Country</label>
				<?php shopp('checkout','shipping-country','required=true&title=Country shipping address'); ?>
				<div class="none"><input type="text" value="n/a" title="Postal/Zip Code shipping address" id="shipping-postcode" name="shipping[postcode]"></div>
			</div>
			<div class="clear"></div>
			<?php else: ?>
			<?php endif; ?>
			<?php if (shopp('checkout','billing-localities')): ?>
			<?php shopp('checkout','billing-locale'); ?>
			<label for="billing-locale">Local Jurisdiction</label>
			<?php endif; ?>
			<?php shopp('checkout','payment-options'); ?>
			<?php shopp('checkout','gateway-inputs'); ?>
			<?php if (shopp('checkout','card-required')): ?>
			<h3>Payment Information</h3>
			<div class="left-form">
				<label for="billing-card">Credit/Debit Card Number</label>
				<?php shopp('checkout','billing-card','required=true&size=30&title=Credit/Debit Card Number'); ?>
			</div>
			<div class="right-form">
				<label for="shipping-country">Country</label>
				<?php shopp('checkout','shipping-country','required=true&title=Country shipping address'); ?>
			</div>
			<div class="clear"></div>
			<div class="left-form">
				<label for="billing-card">MM / YY</label>
				<?php shopp('checkout','billing-cardexpires-mm','size=4&required=true&minlength=2&maxlength=2&title=Card\'s 2-digit expiration month'); ?>
				<?php shopp('checkout','billing-cardexpires-yy','size=4&required=true&minlength=2&maxlength=2&title=Card\'s 2-digit expiration year'); ?>
			</div>
			<div class="right-form">
				<label for="billing-cardtype">Card Type</label>
				<?php shopp('checkout','billing-cardtype','required=true&title=Card Type'); ?>
			</div>
			<div class="clear"></div>
			<div class="left-form">
				<label for="billing-cardholder">Name on Card</label>
				<?php shopp('checkout','billing-cardholder','required=true&size=30&title=Card Holder\'s Name'); ?>
			</div>
			<div class="right-form">
				<label for="billing-cvv">Security ID</label>
				<?php shopp('checkout','billing-cvv','size=7&minlength=3&maxlength=4&title=Card\'s security code (3-4 digits on the back of the card)'); ?>
			</div>
			<div class="clear"></div>
			<?php if (shopp('checkout','billing-xcsc-required')): // Extra billing security fields ?>
			<div class="left-form">
				<label for="billing-xcsc-start">Start Date</label>
				<?php shopp('checkout','billing-xcsc','input=start&size=7&minlength=5&maxlength=5&title=Card\'s start date (MM/YY)'); ?>
			</div>
			<div class="right-form">
				<label for="billing-xcsc-issue">Issue #</label>
				<?php shopp('checkout','billing-xcsc','input=issue&size=7&minlength=3&maxlength=4&title=Card\'s issue number'); ?>
			</div>
			<div class="clear"></div>
			<?php endif; ?>
			<?php endif; ?>
			<br/>
			<div class="form-button"><button type="submit" name="process" id="checkout-button" >Submit Order</button></div>
			<?php endif; ?>
		</form>
		</div>
	</div>
	<div id="sidebar">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>

<!--//End Product--> 
