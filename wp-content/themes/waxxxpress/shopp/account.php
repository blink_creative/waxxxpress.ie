<!--//Breadcrumbs-->

<div id="breadcrumbs" class="wrapper">
	<?php shopp('catalog','breadcrumb'); ?>
</div>

<!--//End Breadcrumbs--> 

<!--//Product-->

<div id="subpage-container">
	<div id="left-column">
		<ul id="shop-manage">
			<li><a href="<?php echo get_permalink(2693); ?>">Order Support</a></li>
			<?php while (shopp('storefront','account-menu')): ?>
				<?php if ( shopp('customer','management', 'return=true') == 'Downloads' ) continue; ?>		
				<li><a href="<?php shopp('storefront','account-menuitem','url'); ?>"><?php shopp('storefront','account-menuitem'); ?></a></li>
			<?php endwhile; ?>
		</ul>
	</div>
	<div id="sidebar">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>
