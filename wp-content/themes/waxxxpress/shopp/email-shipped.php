Content-type: text/html; charset=utf-8
From: <?php shopp('purchase','email-from'); ?>
To: <?php shopp('purchase','email-to'); ?>
Subject: <?php shopp('purchase','email-subject'); ?>

<html>

<table cellpadding="0" cellspacing="0" bgcolor="#efefef">
	<td>

<div id="header">
	<table style="padding:0 20px 20px; border-bottom:2px dotted #e01881;" bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0">
		<tr>
			<td></td>
		</tr>
		<tr>
			<td width="337"><img src="<?php bloginfo('template_url'); ?>/images/emails/ready.gif" width="297" height="174" /></td>
			<td width="261" style="padding:20px 0 0;" valign="top">
				<p style="text-align:right; color:#E10981; font-family:'Courier New', Courier, monospace; font-size:12px;">
					BAAM Cosmetics<br />
					1 Willington Avenue<br />
					Templeogue<br />
					Dublin 6W<br />
					01 4507573 / 087 2427510<br />
					<br />
						<a href="mailto:info@baamcosmetics.ie" style="color:#E10981; text-decoration:none;">info@baamcosmetics.ie</a><br />
					VAT No. IE3971499K
				</p>
			</td>
		</tr>
	</table>
			
	<div class="content">
		<p>Hi <?php shopp('purchase','firstname'); ?>,</p>
		<p>Your order has been processed and below is confirmation of all your in-flight itinerary details. </p>
		<p>To ensure your Waxxxpress experience is fast and turbulence free, our propeller-powered staff pick, pack and despatch your order with the speed and efficiency of a turbo jet. </p>
		<p>To view your order and progress status, simply log into your Waxxxpress account where you can keep track of all the details from take-off to touch-down.</p>
		<p>So sit back, relax and enjoy the wax ride.</p>
		<p><span>xxx</span></p>
	</div>
</div>
<div id="body">

<div id="receipt" class="shopp">

<table class="labels"><tr>
<td><fieldset class="shipping">
<legend><?php _e('Ship to','Shopp'); ?></legend>
	<address><big><?php shopp('purchase','shipname'); ?></big><br /><br />
	<?php shopp('purchase','shipaddress'); ?><br />
	<?php shopp('purchase','shipxaddress'); ?>
	<?php shopp('purchase','shipcity'); ?>, <?php shopp('purchase','shipstate'); ?> <?php shopp('purchase','shippostcode'); ?><br />
	<?php shopp('purchase','shipcountry'); ?></address>
</fieldset></td>
<td><fieldset class="shipping">
	<legend><?php _e('Shipment','Shopp'); ?></legend>
	<table class="transaction">
		<tr><th><?php _e('Tracking Number:','Shopp'); ?></th><td><?php shopp('purchase','email-event','name=tracking&link=on'); ?></td></tr>
		<tr><th><?php _e('Carrier:','Shopp'); ?></th><td><?php shopp('purchase','email-event','name=carrier'); ?></td></tr>
		<tr><th><?php _e('Order Date:','Shopp'); ?></th><td><?php shopp('purchase','date'); ?></td></tr>
	</table>
</fieldset></td>
</tr></table>

<?php if (shopp('purchase','hasitems')): ?>
<table class="order widefat">
	<thead>
	<tr>
		<th scope="col" class="item"><?php _e('Items','Shopp'); ?></th>
		<th scope="col"><?php _e('Quantity','Shopp'); ?></th>
	</tr>
	</thead>

	<?php while(shopp('purchase','items')): ?>
		<tr>
			<td><?php shopp('purchase','item-name'); ?><?php shopp('purchase','item-options','before= – '); ?><br />
				<?php shopp('purchase','item-sku')."<br />"; ?>
				<?php shopp('purchase','item-addons-list'); ?>
				</td>
			<td><?php shopp('purchase','item-quantity'); ?></td>
		</tr>
	<?php endwhile; ?>

</table>

<?php endif; ?>
</div>

</div>

</html>