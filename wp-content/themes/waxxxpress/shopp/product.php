<?php if (shopp('product','found')): ?>

	<!--//Breadcrumbs-->
	
		<div id="breadcrumbs" class="wrapper">
			<?php shopp('catalog','breadcrumb'); ?>
		</div>
	
	<!--//End Breadcrumbs-->

	<!--//Product-->
	
		<div id="subpage-container">
			<div id="left-column">
				<div id="product-image" class="float-left">
					<a id="zoom" class="cloud-zoom" rel="adjustX: 10, adjustY:-4" href="<?php shopp('product','image','index=2&size=original&property=url'); ?>"><?php shopp('product','coverimage','size=original'); ?></a>	
					<div class="product-zoom"></div>
				</div>
				<div id="product-details" class="float-right">
					<h1><?php shopp('product','name'); ?></h1>
					<h2><?php shopp('product','category'); ?></h2>
					<?php if (shopp('product','onsale')): ?>
						<div class="clear">
							<h4 class="original price">Was <?php shopp('product','price'); ?></h4>
							<h4 class="sale price">Now <?php shopp('product','saleprice'); ?> Ex. Vat</h4>
							<div class="clear"></div>
						</div>
						<?php if (shopp('product','has-savings')): ?>
							<p class="savings"><?php _e('You save','Shopp'); ?> <?php shopp('product','savings'); ?> (<?php shopp('product','savings','show=%'); ?>)!</p>
						<?php endif; ?>
					<?php else: ?>
						<h4 class="price"><?php shopp('product','price'); ?> Ex. Vat</h4>
					<?php endif; ?>
				
					<?php if (shopp('product','freeshipping')): ?>
					<p class="freeshipping"><?php _e('Free Shipping!','Shopp'); ?></p>
					<?php endif; ?>
					<form class="clear" id="product-cart" method="get" action="">
						<fieldset>
							<div class="float-left">
								<label for="quantity">Quantity</label>
								<?php shopp('product','quantity','class=selectall&input=menu'); ?>
							</div>
							<?php if(shopp('product','has-variations')): ?>
							<ul class="variations float-left">
								<?php shopp('product','variations','mode=multiple&label=true&defaults='.__('Select an option','Shopp').''); ?>
							</ul>
							<?php endif; ?>
							<?php if(shopp('product','has-addons')): ?>
								<ul class="addons">
									<?php shopp('product','addons','mode=menu&label=true&defaults='.__('Select an add-on','Shopp').'&before_menu=<li>&after_menu=</li>'); ?>
								</ul>
							<?php endif; ?>
							<div class="clear form-button"><?php shopp('product','addtocart'); ?></div>
						</fieldset>
					</form>
				</div>
				<div class="clear"></div>
				<div id="coda-nav-1">
					<ul id="product-menu">
						<li class="tab1"><a href="#1">Product Details</a></li>
						<li class="tab2"><a href="#2">Instructions</a></li>
						<li class="tab3"><a href="#3">Shipping</a></li>
						<li class="tab4 end"><a href="#4">Your Guarantee</a></li>
					</ul>
				</div>	
				<div id="coda-slider-1">
					<div id="product-content" class="coda-slider">
						<div id="tab-one" class="panel">
							<?php shopp('product','description'); ?>
						</div>
						<div id="tab-two" class="panel">
							<?php shopp('product','summary'); ?>
						</div>
						<div id="tab-three" class="panel">
							<p><strong>Our products to you</strong><br/>As soon as your order is dispatched from our Warehouse, you will be sent an email to confirm that it's on its way. Our Warehouse team works around the clock so you could receive the email in the day or the night. From there it should be delivered within the timescale we advertise. </p>
						</div>
						<div id="tab-four" class="panel">
							<p><strong>Shipping</strong><br/>Once we receive your order, we will dispatch your wax with speed and efficiency. Departure is direct from our Irish warehouse for Arrival at your salon, so you get the wax you need, FAST! Please see Terms &amp; Conditions for additional delivery information</p>
							<p><strong>Your Guarantee</strong><br/>Exxxpress service is what we do best, however if something goes wrong, and occasionally it does, resolving your issue is our priority! Please submit your query via the Order Support form in the Accounts section and one of our helpful Waxxxpress representatives will be in contact with you.</p>
						</div>
					</div>
				</div>
			</div>
			<div id="sidebar">
				<?php get_sidebar(); ?>
			</div>
			<div class="clear"></div>
		</div>
	
	<!--//End Product-->
		
	<?php else: ?>
		<h1>Product Not Found</h1>
		<p>Sorry! The product you requested is not found in our catalog!</p>
	<?php endif; ?>
	

