	
	<!--//Breadcrumbs-->
	
		<div id="breadcrumbs" class="wrapper">
			<?php shopp('catalog','breadcrumb'); ?>
		</div>
	
	<!--//End Breadcrumbs-->
	
	<!--//Categories-->
		
		<div id="categories" class="catalog">
			<div class="category-copy">
				<h1><span class="hoefler">Fly High with our range</h1>
				<div class="akzidenz">With a waxxx to suit everybody, our comprehensive range of products and pre &amp; post care will be sure to send your clients beyond the mile high.</div>
			</div>
			<div class="product-list">
				<ul>
					<li id="catalog-ouioui">
						<a href="<?php echo site_url(); ?>/?shopp_category=hot-wax"><img src="<?php bloginfo('template_url'); ?>/images/category/catalog/ultra.jpg" alt="Oui Oui Wax" /></a>
						<div class="title">
							<h2>Hot Waxxx</h2>
							<h3>Ideal for small &amp; delicate areas</h3>
						</div>
						<div class="tell-more">
							<a href="<?php echo site_url(); ?>/?shopp_category=hot-wax">Tell me more</a>
						</div>
					</li>
					<li id="catalog-lucky">
						<a href="<?php echo site_url(); ?>/?shopp_category=strip-waxxx"><img src="<?php bloginfo('template_url'); ?>/images/category/catalog/lucky-wax.jpg" alt="Lucky Wax" /></a>
						<div class="title">
							<h2>Strip Waxxx</h2>
							<h3>Ideal for large areas of the body</h3>
						</div>
						<div class="tell-more">
							<a href="<?php echo site_url(); ?>/?shopp_category=strip-waxxx">Tell me more</a>
						</div>
					</li>
					<li id="catalog-strips">
						<a href="<?php echo site_url(); ?>/?shopp_category=strips"><img src="<?php bloginfo('template_url'); ?>/images/category/catalog/strips.jpg" alt="Strips" /></a>
						<div class="title">
							<h2>Strips</h2>
							<h3>Italian Spun, High Quality Strips</h3>
						</div>
						<div class="tell-more">
							<a href="<?php echo site_url(); ?>/?shopp_category=strips">Tell me more</a>
						</div>
					</li>
					<li id="catalog-marks">
						<a href="<?php echo site_url(); ?>/?shopp_category=pre-post"><img src="<?php bloginfo('template_url'); ?>/images/category/catalog/marks.jpg" alt="On your Marks" /></a>
						<div class="title">
							<h2>Pre &amp; Post Care</h2>
							<h3>A Comprehensive range of pre &amp; post care products</h3>
						</div>
						<div class="tell-more">
							<a href="<?php echo site_url(); ?>/?page_id=60&shopp_category=3">Tell me more</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	
	<!--//End Categories-->

