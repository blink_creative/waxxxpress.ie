<?php if(shopp('category','hasproducts','load=prices,images,specs')): ?>

	<!--//Breadcrumbs-->
	
		<div id="breadcrumbs" class="wrapper">
			<?php shopp('catalog','breadcrumb'); ?></li>
		</div>
	
	<!--//End Breadcrumbs-->
	
	
	<!--//Categories-->
	
		<div id="categories" class="category-seven">
			<div class="product-list">
				<ul>
					<?php while(shopp('category','products')): ?>
						<li id="product-<?php shopp('product','id'); ?>">
							<a href="<?php shopp('product','url'); ?>"><?php shopp('product','image','index=1&size=original'); ?></a>
							<div class="title">
								<h2><?php shopp('product','spec','name=title'); ?></h2>
								<h3><?php shopp('product','spec','name=subtitle'); ?></h3>
							</div>
							<div class="tell-more">
								<a href="<?php shopp('product','url'); ?>">Tell me more</a>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
			</div>
			<div id="why">
				<div id="circle-bg" class="circle">
					<div class="circle">
						<h1><span class="akzidenz">Waxing</span><br /><span class="hoefler"> Starter Kit</span></h1>
						<p class="akzidenz">Give your waxxxing a smooth take off. The Waxxxing Starter Kit will introduce you to Waxxxpress's waxes and accessories and elevate your waxing to the highest heights!</p>
					</div>
				</div>
			</div>
		</div>
	
	<!--//End Categories-->

<?php else: ?>
	<?php if (!shopp('catalog','is-landing')): ?>
	<?php shopp('catalog','breadcrumb'); ?>
	<h3><?php shopp('category','name'); ?></h3>
	<p>No products were found.</p>
	<?php endif; ?>
<?php endif; ?>









