<?php if (shopp('product','found')): ?>
	<li class="sideproduct"> 
		<div class="product-image"><a href="<?php shopp('product','url'); ?>"><?php shopp('product','coverimage','setting=thumbnails'); ?></a></div>
		<div class="product-details">
			<h3><a href="<?php shopp('product','url'); ?>"><?php shopp('product','name'); ?></a></h3>
			<?php  if(shopp('product','has-categories')) {?><h4><?php shopp('product','category'); ?></h4><?php } ?>
			<?php if (shopp('product','onsale')): ?>
				<h5 class="sale price"><?php shopp('product','saleprice'); ?></h5>
				<?php else: ?>
				<h5 class="price"><?php shopp('product','price'); ?></h5>
			<?php endif; ?>
		</div>
		<div class="clear"></div>
	</li>
<?php endif; ?>