<style type="text/css" media="print">
#receipt-header 
{
    display: block !important;
    height: 123px;
	margin:0 0 20px;
    width: 604px;
}
</style>

<div id="receipt-header" style="display:none;"><img src="http://www.waxxxpress.com/wp-content/themes/waxxxpress/images/receipt-header.jpg" /></div>

<div id="receipt" class="shopp">
<table class="transaction">
	<tr><th class="akzidenz" align="left">Order Num:</th><td><?php shopp('purchase','id'); ?></td></tr>
	<tr><th class="akzidenz" align="left">Order Date:</th><td><?php shopp('purchase','date'); ?></td></tr>
	<tr><th class="akzidenz" align="left">Billed To:</th><td><?php shopp('purchase','card'); ?> (<?php shopp('purchase','cardtype'); ?>)</td></tr>
	<tr><th class="akzidenz" align="left">Transaction:</th><td><?php shopp('purchase','transactionid'); ?> (<strong><?php shopp('purchase','payment'); ?></strong>)</td></tr>
</table>
<br/>
<table width="650" class="labels"><tr>
<td valign="top"><fieldset id="billed-to" class="billing">
	<h4 class="akzidenz">Customer Details</h4>
	<address><big><?php shopp('purchase','firstname'); ?> <?php shopp('purchase','lastname'); ?></big><br/><br />
	<strong>Company:</strong> <?php shopp('purchase','company'); ?><br />
	<strong>E-Mail:</strong> <?php shopp('purchase','email'); ?><br />
	<strong>Phone:</strong> <?php shopp('purchase','phone'); ?><br /><br />
	<?php shopp('purchase','address'); ?><br />
	<?php shopp('purchase','xaddress'); ?>
	<?php shopp('purchase','city'); ?>, <?php shopp('purchase','state'); ?> <?php shopp('purchase','postcode'); ?><br />
	<?php shopp('purchase','country'); ?></address>
	
</fieldset></td>
<td valign="top"><fieldset class="shipping">
		<h4 class="akzidenz">Shipping Address</h4>
		<address>
		<?php shopp('purchase','shipaddress'); ?><br />
		<?php shopp('purchase','shipxaddress'); ?>
		<?php shopp('purchase','shipcity'); ?>, <?php shopp('purchase','shipstate'); ?> <?php shopp('purchase','shippostcode'); ?><br />
		<?php shopp('purchase','shipcountry'); ?></address>
		</fieldset></td>
</tr></table>
<br/><br/>
<?php if (shopp('purchase','hasitems')): ?>
<table id="order-history" class="order widefat receipt">
	<thead>
	<tr>
		<th width="250" scope="col" class="order-date" align="left">Items Ordered</th>
		<th width="100" scope="col" class="order-number" align="left">Quantity</th>
		<th width="150" scope="col" class="order-status" align="left">Item Price</th>
		<th width="150" scope="col" class="order-total" align="left">Item Total</th>
	</tr>
	</thead>

	<?php while(shopp('purchase','items')): ?>
		<tr>
			<td><?php shopp('purchase','item-name'); ?><?php shopp('purchase','item-options','before= – '); ?><br />
				<?php shopp('purchase','item-sku')."<br />"; ?>
				<?php shopp('purchase','item-download'); ?>
				<?php shopp('purchase','item-addons-list'); ?>
				</td>
			<td><?php shopp('purchase','item-quantity'); ?></td>
			<td class="money"><?php shopp('purchase','item-unitprice','taxes=false'); ?></td>
			<td class="money"><?php shopp('purchase','item-total','taxes=false'); ?></td>
		</tr>
	<?php endwhile; ?>

	<tr class="totals">
		<th align="left" scope="row" colspan="3" class="total">Subtotal</th>
		<td class="money"><?php shopp('purchase','subtotal'); ?></td>
	</tr>
	<?php if (shopp('purchase','hasdiscount')): ?>
	<tr class="totals">
		<th align="left"  scope="row" colspan="3" class="total">Discount</th>
		<td class="money">-<?php shopp('purchase','discount'); ?></td>
	</tr>
	<?php endif; ?>
	<?php if (shopp('purchase','hasfreight')): ?>
	<tr class="totals">
		<th align="left"  scope="row" colspan="3" class="total">Shipping</th>
		<td class="money"><?php shopp('purchase','freight'); ?></td>
	</tr>
	<?php endif; ?>
	<?php if (shopp('purchase','hastax')): ?>
	<tr class="totals">
		<th align="left"  scope="row" colspan="3" class="total">VAT (23%)</th>
		<td class="money"><?php shopp('purchase','tax'); ?></td>
	</tr>
	<?php endif; ?>
	<tr class="totals">
		<th align="left"  scope="row" colspan="3" class="total">Total</th>
		<td class="money"><?php shopp('purchase','total'); ?></td>
	</tr>
</table>

<?php if(shopp('purchase','has-data')): ?>
	<ul>
	<?php while(shopp('purchase','orderdata')): ?>
		<?php if (shopp('purchase','data','echo=0') == '') continue; ?>
		<li><strong><?php shopp('purchase','data','name'); ?>:</strong> <?php shopp('purchase','data'); ?></li>
	<?php endwhile; ?>
	</ul>
<?php endif; ?>
	

<?php else: ?>
	<p class="warning">There were no items found for this purchase.</p>
<?php endif; ?>
</div>
