<!--//Breadcrumbs-->

<div id="breadcrumbs" class="wrapper">
	<?php shopp('catalog','breadcrumb'); ?>
</div>

<!--//End Breadcrumbs--> 

<!--//Product-->

<div id="subpage-container">
	<div id="left-column">
		<?php if (shopp('purchase','get-id')): ?>
			<?php shopp('purchase','receipt'); ?>
		<?php return; endif; ?>
		<form id="checkout" action="<?php shopp('customer','action'); ?>" method="post" class="shopp validate" autocomplete="off">
			<h1>Your Order History</h1>
			<h3>View previous orders</h3>
			<?php if (shopp('customer','has-purchases')): ?>
				<table cellspacing="0" cellpadding="0">
					<thead>
						<tr>
						<th scope="col" class="order-date" align="left">Date</th>
						<th scope="col" class="order-number" align="left">Order</th>
						<th scope="col" class="order-status" align="left">Status</th>
						<th scope="col" class="order-total" align="left">Total</th>
						</tr>
					</thead>
					<?php while(shopp('customer','purchases')): ?>
					<tr>
						<td><?php shopp('purchase','date'); ?></td>
						<td><?php shopp('purchase','id'); ?></td>
						<td><?php shopp('purchase','status'); ?></td>
						<td><?php shopp('purchase','total'); ?></td>
						<td><a href="<?php shopp('customer','order'); ?>">View Order</a></td>
					</tr>
					<?php endwhile; ?>
				</table>
			<?php else: ?>
			<p>You have no orders, yet.</p>
			<?php endif; // end 'has-purchases' ?>
		</form>
		<p><a href="<?php shopp('customer','url'); ?>">&laquo; Return to Account Management</a></p>
	</div>
	<div id="sidebar">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>
