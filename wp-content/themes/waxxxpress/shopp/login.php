<!--//Breadcrumbs-->

<div id="breadcrumbs" class="wrapper">
	<?php shopp('catalog','breadcrumb'); ?>
</div>

<!--//End Breadcrumbs--> 

<!--//Product-->

<div id="subpage-container">
	<div id="left-column">
		<?php if (shopp('customer','accounts','return=true') == "none"): ?>
		<?php shopp('customer','order-lookup'); ?>
		<?php return; endif; ?>
		<form action="<?php shopp('customer','url'); ?>" method="post" class="shopp password-forgot" id="checkout">
			<div id="checkout">
				<?php if (shopp('customer','notloggedin')): ?>
				<?php shopp('customer','login-errors'); ?>
				<h3>Account Login</h3>
				<div class="left-form">
					<label for="login">
						<?php shopp('customer','login-label'); ?>
					</label>
					<?php shopp('customer','account-login','size=20&title=Login'); ?>
				</div>
				<div class="right-form">
					<label for="password">Password</label>
					<?php shopp('customer','password-login','size=20&title=Password'); ?>
				</div>
				<?php shopp('customer','login-button'); ?>
				<div class="clear"></div>
				<p><a href="<?php shopp('customer','recover-url'); ?>">Lost your password?</a></p>
				<?php endif; ?>
			</div>
		</form>
	</div>
	<div id="sidebar">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>

<!--//End Product--> 
