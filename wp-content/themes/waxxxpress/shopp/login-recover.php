<!--//Breadcrumbs-->

<div id="breadcrumbs" class="wrapper">
	<?php shopp('catalog','breadcrumb'); ?>
</div>

<!--//End Breadcrumbs--> 

<!--//Product-->

<div id="subpage-container">
	<div id="left-column">
		<?php if (shopp('customer','accounts','return=true') == "none"): ?>
		<?php shopp('customer','order-lookup'); ?>
		<?php return; endif; ?>
		<form action="<?php shopp('customer','url'); ?>" method="post" class="shopp" id="checkout">
			<h3>Recover your password</h3>
			<div class="form-left">
				<label for="login">
					<?php shopp('customer','login-label'); ?>
				</label>
				<?php shopp('customer','account-login','size=20&title=Login'); ?>
			</div>
			<div class="form-button">
				<?php shopp('customer','recover-button'); ?>
			</div>
		</form>
	</div>
	<div id="sidebar">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>

<!--//End Product--> 
