<?php if (shopp('cart','hasitems')): ?>
	<!--//Breadcrumbs-->
	
		<div id="breadcrumbs" class="wrapper">
			<?php shopp('catalog','breadcrumb'); ?>
		</div>
	
	<!--//End Breadcrumbs-->

		<!--//Product-->
		
			<div id="subpage-container">
				<div id="left-column">
					<?php if (shopp('cart','hasitems')): ?>
					
						<form id="cart" action="<?php shopp('cart','url'); ?>" method="post">
							<?php shopp('cart','function'); ?>
							<table class="cart">
								<tr>
									<th scope="col" class="item">Cart Items</th>
									<th scope="col">Quantity</th>
									<th scope="col" class="money">Item Price</th>
									<th scope="col" class="money">Item Total</th>
								</tr>
							
								<?php while(shopp('cart','items')): ?>
									<tr>
										<td>
											<div class="cart-image float-left">
												<?php shopp('cartitem','coverimage','size=48'); ?>
											</div>
											<div class="float-left cart-title">
												<a href="<?php shopp('cartitem','url'); ?>"><?php shopp('cartitem','name'); ?></a>
												<?php shopp('cartitem','options'); ?>
												<?php shopp('cartitem','addons-list'); ?>
												<?php shopp('cartitem','inputs-list'); ?>
											</div>
											<div class="clear"></div>
										</td>
										<td><?php shopp('cartitem','quantity','input=text'); ?>
											<?php shopp('cartitem','remove','input=button'); ?></td>
										<td class="money"><?php shopp('cartitem','unitprice','taxes=false'); ?></td>
										<td class="money"><?php shopp('cartitem','total','taxes=false'); ?></td>
									</tr>
								<?php endwhile; ?>
							
								<?php while(shopp('cart','promos')): ?>
									<tr><td colspan="4" class="money"><?php shopp('cart','promo-name'); ?><strong><?php shopp('cart','promo-discount',array('before' => '&nbsp;&mdash;&nbsp;')); ?></strong></td></tr>
								<?php endwhile; ?>
								<tr>
									<td height="30"></td>
								</tr>
								<tr class="totals">
									<td colspan="2" rowspan="6">
										<div id="promocode-container">
											<h2>Apply Coupon</h2>
											<?php shopp('cart','promo-code'); ?>
										</div>
									</td>
									<th scope="row">Subtotal</th>
									<td class="money"><?php shopp('cart','subtotal'); ?></td>
								</tr>
								<?php if (shopp('cart','hasdiscount')): ?>
								<tr class="totals">
									<th scope="row">Discount</th>
									<td class="money">-<?php shopp('cart','discount'); ?></td>
								</tr>
								<?php endif; ?>
								<?php if (shopp('cart','needs-shipped')): ?>
								<tr class="totals">
									<th scope="row"><?php shopp('cart','shipping','label='.__('Shipping','Shopp')); ?></th>
									<td class="money"><?php shopp('cart','shipping'); ?></td>
								</tr>
								<?php endif; ?>
								<tr class="totals">
									<th scope="row"><?php shopp('cart','tax','label=Tax'); ?></th>
									<td class="money"><?php shopp('cart','tax'); ?></td>
								</tr>
								<tr>
									<td height="30"></td>
								</tr>
								<tr class="totals total">
									<th scope="row">Total</th>
									<td class="money"><?php shopp('cart','total'); ?></td>
								</tr>
							</table>
							<div class="cart-buttons">
								<div class="float-left form-button">
									<button type="submit" id="update-button" name="update" value="order" class="button-secondary">Update Subtotal</button>
								</div>
								<div class="float-left button-rect-pink">
									<a href="<?php shopp('cart','referrer'); ?>">Continue Shopping</a>
								</div>
								<div class="float-right button-rect-pink">
									<a href="<?php shopp('checkout','url'); ?>" class="right">Proceed to Checkout</a>
								</div>
								<div class="clear"></div>
							</div>
						</form>

					<?php else: ?>
						<p class="warning">There are currently no items in your shopping cart.</p>
						<p><a href="<?php shopp('catalog','url'); ?>">&laquo; Continue Shopping</a></p>
					<?php endif; ?>
				</div>
				<div id="sidebar">
					<?php get_sidebar(); ?>
				</div>
				<div class="clear"></div>
			</div>
		
		<!--//End Product-->

<?php else: ?>
	<p class="warning">There are currently no items in your shopping cart.</p>
	<p><a href="<?php shopp('catalog','url'); ?>">&laquo; Continue Shopping</a></p>
<?php endif; ?>
