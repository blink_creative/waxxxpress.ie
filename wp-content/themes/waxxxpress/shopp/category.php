<?php if(shopp('category','hasproducts','load=prices,images,specs,description')): ?>

	<!--//Breadcrumbs-->
		
	<div id="breadcrumbs" class="wrapper">
		<?php shopp('catalog','breadcrumb'); ?>
	</div>
	
	<!--//End Breadcrumbs-->
	
	
	<!--//Categories-->
	
		<div id="categories">
			<div class="category-copy">
				<h1><span class="hoefler"><?php shopp('category','name'); ?></span></h1>
				<div class="akzidenz"><?php shopp('category','description'); ?></div>
			</div>
			<div class="product-list">
				<ul>
					<?php while(shopp('category','products')): ?>
						<li>
							<a class="product-listing" href="<?php shopp('product','url'); ?>"><?php shopp('product','image','index=0&size=original'); ?></a>
							<div class="title">
								<h2><?php shopp('product','name'); ?></h2>
								<h3><?php shopp('product','spec','name=subtitle'); ?></h3>
							</div>
							<div class="tell-more">
								<a href="<?php shopp('product','url'); ?>">Tell me more</a>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
			</div>
		</div>

	
	<!--//End Product-->
		
<?php else: ?>
		<div id="subpage-container">
			<div id="left-column">
				<h2>Product Not Found</h2>
				<p>Sorry! The product you requested is not found in our catalog!</p>
			</div>
			<div id="sidebar">
				<?php get_sidebar(); ?>
			</div>
			<div class="clear"></div>
		</div>

<?php endif; ?>

