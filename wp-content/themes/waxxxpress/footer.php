<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */
?>

	</div>
</div>
	
<!--//Footer-->

	<div id="frame-footer">
		<div id="navigation-footer">
			<div class="wrapper">
				<ul id="footer-menu">
					<?php wp_list_pages('title_li=&child_of=2705') ?>
				</ul>
				<div id="networks" class="float-right">
					<ul>
						<li class="button-signup">
							<form action="http://blinkcreative.createsend.com/t/r/s/jljyihk/" method="post" id="subForm" class="form">
								<div class="float-left"><input value="Sign up for e-mail offers!" onfocus="if (value =='Sign up for e-mail offers!') value=''" onblur="if (value=='') value='Sign up for e-mail offers!'" type="text" name="cm-jljyihk-jljyihk" id="jljyihk-jljyihk"/></div>
								<div class="form-button float-left"><button type="submit">Sign Up!</button></div>
							</form>						
						</li>
						<li class="button-facebook"><a href="http://www.facebook.com/Waxxxpress">Join Waxxxpress on Facebook</a></li>
						<li class="button-twitter"><a href="https://twitter.com/#!/waxxxpress">Follow Waxxxpress on Twitter</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="frame-copyright" class="clear wrapper">
			<p>&copy; Waxxxpress Pty Ltd. All world wide rights reserved. | <a href="http://www.blinkcreative.com.au" target="_blank">Web Design by Blink</a></p>
		</div>
	</div>
	
<!--//Script-->

	
    <!--[if IE]>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/function/PIE.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			$('.circle').each(function() {
				PIE.attach(this);
			});
		});
		</script>
	 <![endif]-->    
	
	
<!--//Analytics-->


</body>
</html>
