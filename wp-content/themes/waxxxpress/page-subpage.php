<?php
/**
 * Template Name: Subpage
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

	
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
				
					<!--//Breadcrumbs-->
					
						<div id="breadcrumbs" class="wrapper">
							<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ul id="breadcrumb-list"><li>','</li></ul>');
							} ?>
						</div>
					
					<!--//End Breadcrumbs-->
					
					<!--//Categories-->
					
						<div id="subpage-container">
							<div id="left-column">
								<div class="content">
									<h1><?php the_title(); ?></h1>
									<h2><?php the_field('sub_title'); ?></h2>
									<?php the_content(); ?>
								</div>
							</div>
							<div id="sidebar">
						<?php include("sidebar-101.php"); ?>
							</div>
							<div class="clear"></div>
						</div>
					
					<!--//End Categories-->

				</div>
			</div>
				
		<!--//End Content-->

<?php get_footer(); ?>