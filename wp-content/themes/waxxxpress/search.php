<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

	<!--//Content-->
	
		<div id="frame-content">
			<div class="wrapper">
			
				<!--//Breadcrumbs-->
				
					<div id="breadcrumbs" class="wrapper">
						<?php if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<ul id="breadcrumb-list"><li>','</li></ul>');
						} ?>
					</div>
				
				<!--//End Breadcrumbs-->
				
				<!--//Categories-->
				
					<div id="subpage-container">
						<div id="left-column">
							<?php if ( have_posts() ) : ?>
								<h1><?php printf( __( 'Search Results for: %s', 'twentyten' ), '' . get_search_query() . '' ); ?></h1>
								<?php get_template_part( 'loop', 'search' ); ?>
							<?php else : ?>
								<h1><?php _e( 'Nothing Found', 'twentyten' ); ?></h1>
								<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyten' ); ?></p>
							<?php endif; ?>
						</div>
						<div id="sidebar">
							<?php get_sidebar(); ?>
						</div>
						<div class="clear"></div>
					</div>
				
				<!--//End Categories-->

			</div>
		</div>
			
	<!--//End Content-->




<?php get_sidebar(); ?>
<?php get_footer(); ?>
