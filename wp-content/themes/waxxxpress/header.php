<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Waxxxpress
 * @since Waxxxpress 1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<!--//Meta-->

	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="icon" type="image/png"  href="<?php bloginfo('template_url'); ?>/favicon.ico">
	<?php if ( is_singular() ) { ?>
		<link rel="canonical" href="<?php the_permalink(); ?>" />
	<?php } ?>
    
<!--//Stylesheets-->
    
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/layout.css" />
    <!--[if IE]>
        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/ie.css" />
    <![endif]--> 
	
<!--//Script-->

	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/jquery-core.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/min/jquery-min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-config.js"></script>
	
<!--//Wordpress-->
    
    <?php wp_deregister_script('jquery');wp_head(); ?>
	
</head>

<!--//Begin Site-->

<body <?php body_class(); ?> <?php if ( is_front_page() ) {} else { ?>id="subpage"<?php }; ?>>	
	<div id="site">
		<div id="sticky-footer" class="clearfix">
	
			<!--//Header-->
			
				<div id="frame-header" class="wrapper">
					<div id="logo-waxxxpress" class="float-left">
						<h1><a href="<?php echo site_url(); ?>">Welcome to Waxxxpress</a></h1>
					</div>
					<div id="site-functions" class="float-right">
						<div id="country-switcher">
							Location: Ireland
						</div>
						<div id="site-cart" class="float-right">
							<ul id="cart-menu">
								<?php if(shopp('customer','notloggedin')): ?>
									<li><a href="<?php shopp('customer','url'); ?>">Login</a></li>
								<?php endif; ?>
								<?php if(shopp('customer','loggedin')): ?>
									<li><a href="<?php shopp('customer','url'); ?>">Your Account</a></li>
									<li><a href="/?acct=logout">Logout</a></li>
								<?php endif; ?>
								<li><p><a href="<?php shopp('cart','url'); ?>"><?php shopp('cart','totalitems'); ?> Items in Cart</a></p></li>
							</ul>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
			
			<!--//Navigation-->
			
				<div id="frame-navigation">
                    <div id="navigation-bar" class="float-left" <?php if(is_catalog_page()){?>style="height:90px;"<?php } ?>></div>
					<div class="wrapper">
						<div id="navigation-container">
							<div id="navigation-main">
								<ul class="drop-menu">		
									<li class="drop-down"><a href="<?php shopp('catalog','url'); ?>">Products</a>
										<div class="slide-container product-menu" <?php if(is_catalog_page()){?>style="height:45px;"<?php } ?>>
											<div class="slide-padding">
												<div class="slide-menu">
													<?php wp_nav_menu(); ?>
													<div class="clear"></div>
												</div>
											</div>
										</div>
									</li>
									<li <?php $children = wp_list_pages('title_li=&child_of=25&echo=0'); if ($children) { ?>class="drop-down"<?php } ?>>
										<a href="#">Hints&amp;Tips</a>
										<?php $children = wp_list_pages('title_li=&child_of=25&echo=0'); if ($children) { ?>
												<div class="slide-container" <?php if(is_page(25) || $post->post_parent=="25"){?>style="height:45px;"<?php } ?>>
												<div class="slide-padding">
													<div class="slide-menu">
														<ul class="menu">
															<?php echo $children; ?>
														</ul>
														<div class="clear"></div>
													</div>
												</div>
											</div>
										<?php } ?>									
									</li>
									<li <?php $children = wp_list_pages('title_li=&child_of=23&echo=0'); if ($children) { ?>class="drop-down"<?php } ?> >
										<a href="#">About Us</a>
										<?php $children = wp_list_pages('title_li=&child_of=23&echo=0'); if ($children) { ?>
											<div class="slide-container" <?php if(is_page(23) || $post->post_parent=="23"){?>style="height:45px;"<?php } ?>>
												<div class="slide-padding">
													<div class="slide-menu">
														<ul class="menu">
															<?php echo $children; ?>
														</ul>
														<div class="clear"></div>
													</div>
												</div>
											</div>
										<?php } ?>									
									</li>
									<li <?php $children = wp_list_pages('title_li=&child_of=27&echo=0'); if ($children) { ?>class="drop-down"<?php } ?>>
										<a href="<?php echo get_permalink(27); ?>">The Buzz</a>
										<?php $children = wp_list_pages('title_li=&child_of=27&echo=0'); if ($children) { ?>
											<div class="slide-container">
												<div class="slide-padding">
													<div class="slide-menu">
														<ul class="menu">
															<?php echo $children; ?>
														</ul>
														<div class="clear"></div>
													</div>
												</div>
											</div>
										<?php } ?>									
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
