<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>
	
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
				
					<!--//Breadcrumbs-->
					
						<div id="breadcrumbs" class="wrapper">
							<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ul id="breadcrumb-list"><li>','</li></ul>');
							} ?>
						</div>
					
					<!--//End Breadcrumbs-->
					
					<!--//Categories-->
					
						<div id="subpage-container">
							<div id="left-column">
								<div class="content">
									<h1><?php printf( __( 'Category Archives: %s', 'twentyten' ), '' . single_cat_title( '', false ) . '' ); ?></h1>
									<?php
										$category_description = category_description();
										if ( ! empty( $category_description ) )
											echo '' . $category_description . '';
										get_template_part( 'loop', 'category' );
									?>
								</div>
							</div>
							<div id="sidebar">
								<?php include('sidebar-blog.php'); ?>
							</div>
							<div class="clear"></div>
						</div>
					
					<!--//End Categories-->

				</div>
			</div>
				
		<!--//End Content-->
		

<?php get_footer(); ?>
