<?php
/**
 * Template Name: Waxxxselector
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
				
					<!--//Breadcrumbs-->
					
						<div id="breadcrumbs" class="wrapper">
							<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ul id="breadcrumb-list"><li>','</li></ul>');
							} ?>
						</div>
					
					<!--//End Breadcrumbs-->
					
					<!--//Categories-->
					
						<div id="subpage-container">
							<div id="left-column">
								<div class="content">
									<h1><?php the_title(); ?></h1>
									<h2><?php the_field('sub_title'); ?></h2>
									
									<script type="text/javascript">
									
									$(document).ready(function() {
									
    									$('#questionone').change(function(event) {
											if($(this).val() == 'Hot Wax'){
											  $('.strip').css('display','none');
    										  $('.hot').fadeIn('slow', function() {}); 
												if($('#questiontwo').val() == 'Coarse hair'){
													  $('.sensitive').css('display','none');
												}
												if($('#questiontwo').val() == 'Sensitive skin'){
													  $('.coarse').css('display','none');
												}
      										}
   										 }); 
    									$('#questionone').change(function(event) {
											if($(this).val() == 'Strip Wax'){
											  $('.hot').css('display','none');
    										  $('.strip').fadeIn('slow', function() {}); 
												if($('#questiontwo').val() == 'Coarse hair'){
													  $('.sensitive').css('display','none');
												}
												if($('#questiontwo').val() == 'Sensitive skin'){
													  $('.coarse').css('display','none');
												}
      										}
   										 }); 
    									$('#questiontwo').change(function(event) {
											if($(this).val() == 'Coarse hair'){
											  $('.sensitive').css('display','none');
    										  $('.coarse').fadeIn('slow', function() {}); 
												if($('#questionone').val() == 'Hot Wax'){
													  $('.strip').css('display','none');
												}
												if($('#questionone').val() == 'Strip Wax'){
													  $('.hot').css('display','none');
												}
      										}
   										 }); 
    									$('#questiontwo').change(function(event) {
											if($(this).val() == 'Sensitive skin'){
											  $('.coarse').css('display','none');
    										  $('.sensitive').fadeIn('slow', function() {}); 
												if($('#questionone').val() == 'Hot Wax'){
													  $('.strip').css('display','none');
												}
												if($('#questionone').val() == 'Strip Wax'){
													  $('.hot').css('display','none');
												}
      										}
   										 }); 
   										 									
									
									});
									
									</script>
									
									
									<div id="waxxxselector">
										<h3 class="akzidenz">Step 1. What type of wax are you after?</h3>
										<select id="questionone">
											<option>Select Option</option>
											<option id="subone">Hot Wax</option>
											<option id="subtwo">Strip Wax</option>
										</select>
										<h3 class="akzidenz">Step 2. What is more important to target?</h3>
										<select id="questiontwo">
											<option>Select Option</option>
											<option id="subthree">Coarse hair</option>
											<option id="subfour">Sensitive skin</option>
										</select>
										<div class="clear"></div>
										<br/><br/>
										<h2>Drumroll… Your wax is...</h2>										
										<div id="final-select" class="clear">
											<div class="coco hot sensitive"><a href="<?php echo home_url( '/' ); ?>?page_id=60&shopp_pid=1"><img src="http://www.waxxxpress.com/wp-content/themes/waxxxpress/images/waxselector/coconuts.jpg"/></a></div>
											<div class="fresh hot sensitive"><a href="<?php echo home_url( '/' ); ?>?page_id=60&shopp_pid=3"><img src="http://www.waxxxpress.com/wp-content/themes/waxxxpress/images/waxselector/fresh.jpg"/></a></div>
											<div class="gold hot coarse"><a href="<?php echo home_url( '/' ); ?>?page_id=60&shopp_pid=4"><img src="http://www.waxxxpress.com/wp-content/themes/waxxxpress/images/waxselector/gold.jpg"/></a></div>
											<div class="ultra hot coarse"><a href="<?php echo home_url( '/' ); ?>?page_id=60&shopp_pid=2"><img src="http://www.waxxxpress.com/wp-content/themes/waxxxpress/images/waxselector/ultra.jpg"/></a></div>
											<div class="heat strip sensitive"><a href="<?php echo home_url( '/' ); ?>?page_id=60&shopp_pid=5"><img src="http://www.waxxxpress.com/wp-content/themes/waxxxpress/images/waxselector/heat-me.jpg" /></a></div>
											<div class="lucky strip coarse"><a href="<?php echo home_url( '/' ); ?>?page_id=60&shopp_pid=6"><img src="http://www.waxxxpress.com/wp-content/themes/waxxxpress/images/waxselector/lucky.jpg"/></a></div>
											<div class="ouioui strip sensitive"><a href="<?php echo home_url( '/' ); ?>?page_id=60&shopp_pid=7"><img src="http://www.waxxxpress.com/wp-content/themes/waxxxpress/images/waxselector/oui.jpg"/></a></div>
											<div class="beeswax strip coarse"><a href="<?php echo home_url( '/' ); ?>?page_id=60&shopp_pid=8"><img src="http://www.waxxxpress.com/wp-content/themes/waxxxpress/images/waxselector/bees.jpg"/></a></div>
										</div>
									</div>
								</div>
							</div>
							<div id="sidebar">
						<?php include("sidebar-101.php"); ?>
							</div>
							<div class="clear"></div>
						</div>
					
					<!--//End Categories-->

				</div>
			</div>
				
		<!--//End Content-->
		
	<?php endwhile; ?>

<?php get_footer(); ?>