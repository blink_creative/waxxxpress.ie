<?php
/**
 * Template Name: Goodbuys
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

	
		<!--//Content-->
		
			<div id="frame-content">
				<div class="wrapper">
				
					<!--//Breadcrumbs-->
					
						<div id="breadcrumbs" class="wrapper">
							<?php if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb('<ul id="breadcrumb-list"><li>','</li></ul>');
							} ?>
						</div>
					
					<!--//End Breadcrumbs-->
					
					<!--//Categories-->
					
						<div id="subpage-container-goodbuys">
							<ul id="goodbuys">
								<?php $i=1; ?>
								<?php $recent = new WP_Query("cat=11&showposts=10"); while($recent->have_posts()) : $recent->the_post();?>
									<?php 
										$i++;
										$class = ($i&1);
									 ?>
									<li class="goodbuy-<?php echo $class; ?>">
										<a href="<?php the_field('good-link'); ?>"><img src="<?php the_field('good-image'); ?>" alt="" /></a>
										<div class="goodbuys-copy">
											<h2><?php the_field('good-title'); ?></h2>
											<p><?php the_field('good-details'); ?></p>
											<div class="button-rect-pink">
												<a href="<?php the_field('good-link'); ?>">Shop Now!</a>
											</div>
										</div>
									</li>
								<?php endwhile; ?>
							</ul>
						</div>
					
					<!--//End Categories-->

				</div>
			</div>
				
		<!--//End Content-->
		

<?php get_footer(); ?>