// JavaScript Document

$(document).ready(function() {
						   
	/*--First level dropdown--*/
	
	$(".drop-menu .drop-down").hover(
	  function () {
		$('#navigation-bar').stop().css('height','90px');
		$(this).find('.slide-container').css('overflow','visible').stop().css('height','45px');
	  }, 
	  function () {
		$('#navigation-bar').stop().css('height','45px');
		$('.drop-menu').find('.slide-container').css('overflow','hidden').stop().css('height','0px');
	  }
	);			
	
	/*--Animations--*/

	window.setTimeout( koala, 1500 );
	window.setTimeout( salon, 2500 );
	window.setTimeout( bridge, 500 );
	function koala(){
		$('#animation-koala').animate({
			left: -216,
			top: -50
		},{
			duration: 2000,
			easing: 'easeOutExpo'			
		});	
	}
	
	function salon(){
		$('#animation-salon').animate({
			top: 0
		},{
			duration: 1000,
			easing: 'easeOutBounce'			
		});	
	}
	function bridge(){
		$('#animation-bridge').animate({
			right: 324
		},{
			duration: 1000,
			easing: 'easeOutBounce'			
		});	
	}
	
	
	/*--Wiggle--*/
	
	$(function() {
		var interval = null;
		$('#animation-koala').hover(function(){
			if($(this).wiggle('isWiggling')) {
				$(this).wiggle('stop');
			} else {
				$(this).wiggle('start');
			}
		});
	});

	$(function() {
		var interval = null;
		$('#animation-heart').hover(function(){
			if($(this).wiggle('isWiggling')) {
				$(this).wiggle('stop');
			} else {
				$(this).wiggle('start');
			}
		});
	});
	
	$(function() {
		var interval = null;
		$('#animation-pointer').hover(function(){
			if($('#pointer-image').wiggle('isWiggling')) {
				$('#pointer-image').wiggle('stop');
			} else {
				$('#pointer-image').wiggle('start');
			}
		});
	});
			

});
