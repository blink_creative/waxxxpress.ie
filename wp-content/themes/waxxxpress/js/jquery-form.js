$(document).ready(function() {
	submitForms = function(){
		document.getElementById("sreg-form").submit();
	}
	
	$('#sreg-firstname').keyup(function () {
  		$('#one').val(this.value);
	});

	$('#sreg-lastname').keyup(function () {
  		$('#two').val(this.value);
	});
	
	$('#sreg-phone').keyup(function () {
  		$('#three').val(this.value);
	});

	$('#sreg-email').keyup(function () {
  		$('#four').val(this.value);
	});

	$('#sreg-company').keyup(function () {
  		$('#five').val(this.value);
	});

	$('#sreg-billing-address').keyup(function () {
  		$('#six').val(this.value);
	});

	$('#sreg-billing-xaddress').keyup(function () {
  		$('#seven').val(this.value);
	});

	$('#sreg-billing-city').keyup(function () {
  		$('#eight').val(this.value);
	});

	$('#sreg-billing-state').keyup(function () {
  		$('#nine').val(this.value);
	});

	$('#sreg-billing-postcode').keyup(function () {
  		$('#ten').val(this.value);
	});

	$('#sreg-billing-country').change(function () {
  		$('#eleven').val(this.value);
	});
	
    $('.show').click(function() {
      $('#additional').fadeIn('slow', function() {
      });
    });	
	
	
});
