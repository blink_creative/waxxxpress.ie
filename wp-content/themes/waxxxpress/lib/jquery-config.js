// JavaScript Document

	

$(document).ready(function() {
	
	
	/*--Superfish--*/
	
	$('ul.sf-menu').superfish({
		autoArrows:  false
	}); 
	
 
	/*--Cufon--*/
	
	Cufon.replace('.akzidenz,#contact h3, #cart table th, #blog-lists h3, #order-history tr , #shop-manage, #faq-links h3, #faq-sections h3, .button-rect-lightblue, #goodbuys li .goodbuys-copy h2, #goodbuys li .goodbuys-copy p, #checkout h3, #left-column h1, .button-rect-black, .product-list li .title h2, .product-list li .title h3, .product-details h3, .product-details h5, #register h3, #product-content h4, .form-button button, #product-menu li, #product-cart label, #product-details h1, #product-details h3, #breadcrumb-list li, #site-cart, .drop-menu > li > a, .sf-menu > li > a,#navigation-footer li, .button-rect-pink, .button-rect-white, .button-rect-purple, .button-rect-green, .button-rect-blue, #homepage-cta h2, #pointer-cta h3', { fontFamily: 'Akzidenz',hover: true });
	Cufon.replace('.nouvelle', { fontFamily: 'Nouvelle' });
	Cufon.replace('.hoefler, #left-column h2, .product-details h4, #product-details h2', { fontFamily: 'Hoefler', hover:true });
	
	
	/*--tabs--*/
	
	$('.coda-slider').codaSlider({
		dynamicArrows: false,
		dynamicTabs: false
	});
 
 	/*--circle--*/
	
	$(function() {
		if (window.PIE) {
			$('.circle').each(function() {
				PIE.attach(this);
			});
		}
	});	
	
	
	/*--regsiter--*/
	
});
