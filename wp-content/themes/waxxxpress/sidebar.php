<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */
?>

<div id="other-products">
	<div class="title">
		<h2><span class="akzidenz">Others</span> <span class="hoefler">bought</span></h2>
	</div>
	<ul>
		<?php dynamic_sidebar( 'primary-widget-area' ); ?>
	</ul>
</div>
<div class="which-wax">
	<div class="which-image"><a href="<?php echo get_permalink(54); ?>">Which waxxx is right for you?</a></div>
	<div class="button-rect-purple">
		<a href="<?php echo get_permalink(54); ?>">Go to waxxxselector</a>
	</div>
</div>




